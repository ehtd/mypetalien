//
//  Colors.swift
//  FindMyPet
//
//  Created by Josh Berlin on 6/23/14.
//  Copyright (c) 2014 Frothy Software. All rights reserved.
//

import UIKit

extension UIColor {
  
  convenience init(rgbValue: UInt) {
    self.init(
      red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
      green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
      blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
      alpha: CGFloat(1.0)
    )
  }
  
  convenience init(rgbValue: UInt, alpha: CGFloat) {
    let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
    let green = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
    let blue = CGFloat(rgbValue & 0x0000FF) / 255.0
    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
  
  // Backgrounds
  class func greyBackgroundColor() -> UIColor { return UIColor(rgbValue: 0xF7F7F7) }
  
  // Tab bar
  class func tabBarTitleFontColor() -> UIColor { return UIColor(rgbValue: 0x626166) }
  class func tabBarTitleSelectedFontColor() -> UIColor { return UIColor(rgbValue: 0x00C752) }
  class func tabBarBackgroundColor() -> UIColor { return UIColor(rgbValue: 0xF5F5F5) }
  class func tabBarTintColor() -> UIColor { return UIColor(rgbValue: 0x00C752) }
  
  // Navigation bar
  class func navigationBarTitleFontColor() -> UIColor { return UIColor(rgbValue: 0xFFFFFF) }
  class func navigationBarBackgroundColor() -> UIColor { return UIColor(rgbValue: 0x00C853) }
  class func navigationBarTintColor() -> UIColor { return UIColor(rgbValue: 0xFFFFFF) }
  
  // Bar button item
  class func barButtonItemTitleFontColor() -> UIColor { return UIColor(rgbValue: 0xFFFFFF, alpha: 0.70) }
  
  // Dividers
  class func dividerColor() -> UIColor { return UIColor(rgbValue: 0xE4E4E4) }
  
  // Table views
  class func cellSeparatorColor() -> UIColor { return UIColor.dividerColor() }
  
  // Dashboard
  class func alienNameTextColor() -> UIColor { return UIColor(rgbValue: 0x656565) }
  class func alienWeightTextColor() -> UIColor { return UIColor(rgbValue: 0x5E5E5E) }
  class func alienLastVisitTextColor() -> UIColor { return UIColor(rgbValue: 0x818181) }
  class func aliensHeaderTextColor() -> UIColor { return UIColor(rgbValue: 0x616266) }
  
}
