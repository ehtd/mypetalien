//
//  AlienDetailView.swift
//  MyPetAlien
//
//  Created by Josh Berlin on 5/20/15.
//  Copyright (c) 2015 CocoaConf. All rights reserved.
//

import UIKit

protocol AlienDetailUserInterface: class {
  func setDisplayableAlien(displayableAlien: DisplayableAlien)
}

class AlienDetailView: UIView {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var weightLabel: UILabel!
  @IBOutlet weak var lastVisitLabel: UILabel!
  
  var displayableAlien: DisplayableAlien! {
    didSet {
      avatarImageView.image = displayableAlien.avatarImage
      weightLabel.text = displayableAlien.weightString
      lastVisitLabel.attributedText = displayableAlien.attributedStringForLastVisit()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    styleView()
  }
  
}

// MARK: Alien detail user interface

extension AlienDetailView: AlienDetailUserInterface {

  func setDisplayableAlien(displayableAlien: DisplayableAlien) {
    self.displayableAlien = displayableAlien
  }
  
}

// MARK: Style

extension AlienDetailView {
  
  func styleView() {
    styleLabels()
  }
  
  func styleLabels() {
    weightLabel.font = UIFont.alienWeightLargeFont()
    weightLabel.textColor = UIColor.alienWeightTextColor()
  }
  
}
