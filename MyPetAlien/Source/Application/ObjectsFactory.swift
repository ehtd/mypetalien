//
//  ObjectsFactory.swift
//  MyPetAlien
//
//  Created by Josh Berlin on 5/19/15.
//  Copyright (c) 2015 CocoaConf. All rights reserved.
//

import Foundation

let objectsFactory = ObjectsFactory()

let fakeAliens = [
  Alien(
    id: "1",
    name: "OctoBaby",
    avatarImageName: "octobaby",
    weight: 18.0,
    lastVisitDate: "3 hours ago"
  ),
  Alien(
    id: "2",
    name: "ChestBurster",
    avatarImageName: "alienbaby",
    weight: 12.0,
    lastVisitDate: "6 hours ago"
  )
]

class ObjectsFactory {
  
  let alienDataAccess = InMemoryAlienDataAccess(aliens: fakeAliens)
  
}
