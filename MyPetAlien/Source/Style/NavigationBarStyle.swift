//
//  NavigationBarStyle.swift
//  Reveler
//
//  Created by Josh Berlin on 2/27/15.
//  Copyright (c) 2015 Everfest. All rights reserved.
//

import UIKit

extension UINavigationBar {
  
  class func styleNavigationBar() {
    let font = UIFont.navigationBarTitleFont()
    UINavigationBar.appearance().titleTextAttributes = [
      NSForegroundColorAttributeName: UIColor.navigationBarTitleFontColor(),
      NSFontAttributeName: font
    ]
    UINavigationBar.appearance().barTintColor = UIColor.navigationBarBackgroundColor()
    UINavigationBar.appearance().tintColor = UIColor.navigationBarTintColor()
    UINavigationBar.appearance().translucent = false
    UIBarButtonItem.styleBarButtonItem()
  }
  
}

extension UIBarButtonItem {
  
  class func styleBarButtonItem() {
    let font = UIFont.barButtonItemTitleFont()
    let attributes = [
      NSForegroundColorAttributeName: UIColor.barButtonItemTitleFontColor(),
      NSFontAttributeName: font
    ]
    UIBarButtonItem.appearance().setTitleTextAttributes(attributes, forState: .Normal)
  }
  
}