//
//  AlienDetailViewController.swift
//  MyPetAlien
//
//  Created by Josh Berlin on 5/20/15.
//  Copyright (c) 2015 CocoaConf. All rights reserved.
//

import UIKit

class AlienDetailViewController: UIViewController {
  
  var alienID: String!
  var userInterface: AlienDetailUserInterface!
  var alienDataAccess: AlienDataAccess = objectsFactory.alienDataAccess
  
  override func viewDidLoad() {
    super.viewDidLoad()
    userInterface = view as! AlienDetailView
    updateAlien()
  }
  
  func updateAlien() {
    if let alien = alienDataAccess.getAlienForID(alienID) {
      let displayableAlien = DisplayableAlien(alien: alien)
      userInterface.setDisplayableAlien(displayableAlien)
      title = alien.name
    }
  }
  
}
