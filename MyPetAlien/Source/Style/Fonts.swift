//
//  Fonts.swift
//  Tailio
//
//  Created by Josh Berlin on 3/30/15.
//  Copyright (c) 2015 Pet Wireless. All rights reserved.
//

import UIKit

extension UIFont {
  
  // Tab bar
  class func tabBarTitleFont() -> UIFont { return UIFont(name: "AvenirNext-Regular", size: 12)! }
  
  // Navigation bar
  class func navigationBarTitleFont() -> UIFont { return UIFont(name: "AvenirNext-Bold", size: 17)! }
  
  // Bar button item
  class func barButtonItemTitleFont() -> UIFont { return UIFont(name: "AvenirNext-Regular", size: 17)! }
  
  // Action button
  class func actionButtonFont() -> UIFont { return UIFont(name: "AvenirNext-DemiBold", size: 15)! }
  
  // Aliens
  class func alienNameFont() -> UIFont { return UIFont(name: "Avenir-Heavy", size: 18)! }
  class func alienWeightLargeFont() -> UIFont { return UIFont(name: "AvenirNext-Medium", size: 18)! }
  class func alienWeightFont() -> UIFont { return UIFont(name: "AvenirNext-Medium", size: 16)! }
  class func alienLastVisitLabelFont() -> UIFont { return UIFont(name: "AvenirNext-Regular", size: 14)! }
  class func alienLastVisitTimeFont() -> UIFont { return UIFont(name: "AvenirNext-Medium", size: 14)! }
  class func aliensHeaderFont() -> UIFont { return UIFont(name: "AvenirNext-Bold", size: 16)! }
  
}
