//
//  AliensViewController.swift
//  MyPetAlien
//
//  Created by Josh Berlin on 5/19/15.
//  Copyright (c) 2015 CocoaConf. All rights reserved.
//

import UIKit

let ShowAlienDetailSegueIdentifier = "ShowAlienDetailSegueIdentifier"

protocol AliensInteractionHandler: class {
  func showAlienDetailForAlien(alienID: String)
}

class AliensViewController: UITableViewController {
  
  var userInterface: AliensUserInterface!
  var alienDataAccess: AlienDataAccess = objectsFactory.alienDataAccess
  
  var selectedAlienID: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Aliens"
    userInterface = view as! AliensTableView
    userInterface.setInteractionHandler(self)
    updateAliens()
  }
  
  func updateAliens() {
    let aliens = alienDataAccess.getAliens()
    let displayableAliens = aliens.map({ DisplayableAlien(alien: $0) })
    userInterface.setDisplayableAliens(displayableAliens)
  }
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return .LightContent
  }
  
}

// MARK: Aliens interaction handler

extension AliensViewController: AliensInteractionHandler {
  
  func showAlienDetailForAlien(alienID: String) {
    selectedAlienID = alienID
    performSegueWithIdentifier(ShowAlienDetailSegueIdentifier, sender: self)
  }
  
}

// MARK: Segues

extension AliensViewController {
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == ShowAlienDetailSegueIdentifier {
      if let alienDetailVC = segue.destinationViewController as? AlienDetailViewController {
        if let alienID = selectedAlienID {
          alienDetailVC.alienID = alienID
        }
      }
    }
  }
}
