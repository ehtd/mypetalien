//
//  AlienDataAccess.swift
//  MyPetAlien
//
//  Created by Josh Berlin on 5/19/15.
//  Copyright (c) 2015 CocoaConf. All rights reserved.
//

import Foundation

protocol AlienDataAccess {
  func getAliens() -> [Alien]
  func getAlienForID(alienID: String) -> Alien?
}
