//
//  AliensHeaderView.swift
//  Tailio
//
//  Created by Josh Berlin on 4/2/15.
//  Copyright (c) 2015 Pet Wireless. All rights reserved.
//

import UIKit

class AliensHeaderView: UIView {
  
  let iconImageView = UIImageView()
  let headerLabel = UILabel()
  
  override init(frame: CGRect) { super.init(frame: frame); setUpView() }
  required init(coder aDecoder: NSCoder) { super.init(coder: aDecoder); setUpView() }
  
  func setUpView() {
    addSubviews()
    styleView()
  }
  
  func addSubviews() {
    addSubview(headerLabel)
    addSubview(iconImageView)
  }
  
  override func layoutSubviews() {
    iconImageView.sizeToFit()
    iconImageView.frame = CGRectMake(10, 0, iconImageView.bounds.size.width, bounds.size.height)
    headerLabel.sizeToFit()
    headerLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame) + 10, 0, headerLabel.bounds.size.width, bounds.size.height)
  }
  
}

// MARK: Style

extension AliensHeaderView {
  
  func styleView() {
    headerLabel.font = UIFont.aliensHeaderFont()
    headerLabel.textColor = UIColor.aliensHeaderTextColor()
    iconImageView.contentMode = .Center
  }
  
}
