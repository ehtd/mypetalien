//
//  AlienCell.swift
//  Tailio
//
//  Created by Josh Berlin on 4/1/15.
//  Copyright (c) 2015 Pet Wireless. All rights reserved.
//

import UIKit

class AlienCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var weightLabel: UILabel!
  @IBOutlet weak var lastVisitLabel: UILabel!
  
  func setDisplayableAlien(displayableAlien: DisplayableAlien) {
    if let image = displayableAlien.avatarImage {
      avatarImageView.image = image
    } else {
      avatarImageView.image = nil
    }
    nameLabel.text = displayableAlien.name
    weightLabel.text = displayableAlien.weightString
    lastVisitLabel.attributedText = displayableAlien.attributedStringForLastVisit()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    styleView()
  }
  
  override var layoutMargins: UIEdgeInsets {
    get {
      return UIEdgeInsetsZero
    }
    set(newVal) { }
  }

}

extension AlienCell {
  
  func styleView() {
    backgroundColor = UIColor.whiteColor()
    styleAvatarImageView()
    styleLabels()
  }
  
  func styleAvatarImageView() {
    avatarImageView.layer.cornerRadius = CGFloat(100.0) / 2.0
    avatarImageView.layer.masksToBounds = true
    avatarImageView.contentMode = .ScaleAspectFill
  }
  
  func styleLabels() {
    nameLabel.font = UIFont.alienNameFont()
    nameLabel.textColor = UIColor.alienNameTextColor()
    weightLabel.font = UIFont.alienWeightFont()
    weightLabel.textColor = UIColor.alienWeightTextColor()
  }
  
}

// MARK: Last visit attributed text

extension DisplayableAlien {
  
  func attributedStringForLastVisit() -> NSAttributedString {
    let lastVisitLabelString = "Last pooped"
    let lastVisitLabelAttributes = [
      NSFontAttributeName : UIFont.alienLastVisitLabelFont(),
      NSForegroundColorAttributeName: UIColor.alienLastVisitTextColor()
    ]
    let lastVisitLabelAttributedString = NSAttributedString(string: lastVisitLabelString, attributes: lastVisitLabelAttributes)
    
    let spaceAttributedString = NSAttributedString(string: " ", attributes: nil)
    
    let lastVisitAttributes = [
      NSFontAttributeName : UIFont.alienLastVisitTimeFont(),
      NSForegroundColorAttributeName: UIColor.alienLastVisitTextColor()
    ]
    let lastVisitAttributedString = NSAttributedString(string: lastVisitString, attributes: lastVisitAttributes)
    
    var attributedString = NSMutableAttributedString()
    attributedString.appendAttributedString(lastVisitLabelAttributedString)
    attributedString.appendAttributedString(spaceAttributedString)
    attributedString.appendAttributedString(lastVisitAttributedString)
    return attributedString
  }
  
}
