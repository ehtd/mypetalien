//
//  Alien.swift
//  Tailio
//
//  Created by Josh Berlin on 4/3/15.
//  Copyright (c) 2015 Pet Wireless. All rights reserved.
//

import UIKit

struct Alien {
  let id: String
  let name: String
  let avatarImageName: String
  let weight: Float
  let lastVisitDate: String
}

struct DisplayableAlien {
  let id: String
  let name: String
  let avatarImage: UIImage?
  let weightString: String
  let lastVisitString: String
  
  init(alien: Alien) {
    id = alien.id
    name = alien.name
    if let image = UIImage(named: alien.avatarImageName) {
      avatarImage = image
    } else {
      avatarImage = nil
    }
    weightString = "\(alien.weight) lbs"
    lastVisitString = alien.lastVisitDate
  }
  
}
